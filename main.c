/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#include <stdint.h>        /* Includes uint16_t definition                    */
#include <stdlib.h>
#include <stdbool.h>       /* Includes true/false definition                  */
#include <stdio.h>

#include "os/system.h"        /* System funct/params, like osc/peripheral config */
#include "os/memory.h"
#include "os/interrupts.h"
#include "extern/platform/e-puck/library/motor_led/e_epuck_ports.h"
#include "os/io/e-puck/proximity.h"

/******************************************************************************/
/* Global Variable Declaration                                                */
/******************************************************************************/



/******************************************************************************/
/* Definitions                                                                */
/******************************************************************************/

// comment out debug definitions when not in debug mode
#define DEBUG_GENERAL       // general debugging

#define SPIN_SPEED 80           // speed for wheels when spinning (limits: -127 -> +127)
#define EXP_SPEED 100           // exploration speed
#define OBJ_FOLLOW_SPEED 50     // speed for wheels when following (limits: 0 -> +127)
#define OBJ_THRESHOLD_MAX 100   // distance threshold for detecting objects
#define OBJ_THRESHOLD_MIN 20    // distance threshold for stop following object

/******************************************************************************/
/* Thread Prototypes                                                          */
/******************************************************************************/

void task1(void);           // task 1 thread
void task2(void);           // task 2 thread
void defaultThread(void);   // no task selected thread

/******************************************************************************/
/* Function Prototypes                                                        */
/******************************************************************************/

void delay(uint32 value);   // blocking delay (ms)
void setMotorSpeeds(sint16 leftMotorSpeed, sint16 rightMotorSpeed); // sets motor speeds
void objDetInd(uint8 data); // object detected indicator
void objNDetInd(uint8 data); // object not detected indicator

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

int16_t main(void) {
    // turn all LEDs off
    LED0 = 0;
    LED1 = 0;
    LED2 = 0;
    LED3 = 0;
    LED4 = 0;
    LED5 = 0;
    LED6 = 0;
    LED7 = 0;
    BODY_LED = 0;
    FRONT_LED = 0;
    
    Sys_Init_Kernel(); // initialise OpenSwarm
    
    if(   !Sys_Start_Process(task1) ||
          !Sys_Start_Process(task2) ||
          !Sys_Start_Process(defaultThread)
      ){ // start threads. If an error occurred turn on the FRONT_LED
        FRONT_LED = 1;
    }
    
    Sys_Start_Kernel(); // start OpenSwarm
    
#ifdef DEBUG_GENERAL
    Sys_Writeto_UART1("OS Started\r\n", 12); // send via Bluetooth
#endif
    
    uint32 time = 1000;
    while(true){ // do nothing (do only things for testing)
        if(SR & 0x00E0){           
            SRbits.IPL = 0;
        }
        if(CORCON & 0x08){
            CORCONbits.IPL3 = 0;
        }
        
        uint32 time_now = Sys_Get_SystemClock();    // get the current time
        if(time_now >= time){                       // if one second passed
            time += 1000;                           // calculate next second
            LED0 = ~LED0;                           // toggle LED0
        }
    }
}

/* Thread: Task 1 */
void task1(void){
    uint selectorValue = 0;     // selector value
    uint proxValues[8];         // proximity sensor values
    int i = 0;                  // loop variable
    
    while(true) {
        if(SR & 0x00E0){           
            SRbits.IPL = 0;
        }
        if(CORCON & 0x08){
            CORCONbits.IPL3 = 0;
        }
        
        selectorValue = Sys_Get_Selector(); // get selector value
        if(selectorValue == 1) {            // if task 1 selected
            // get proximity sensor values
            for (i = 0; i < 8; i++) {
                proxValues[i] = Sys_Get_Prox(i);
                
                // turn on LEDs for the sensors that detect an object
                if (proxValues[i] < 65535)
                    objDetInd(i);
                else
                    objNDetInd(i);
            }

            // if object is obstructing the path and there is no way to avoid it, turn until the path is clear
            if (((proxValues[0] < 50) && (proxValues[7] < 50)) || 
                ((proxValues[1] < 20) && (proxValues[6] < 20)) || 
                ((proxValues[2] < 10) && (proxValues[5] < 10))) {
                setMotorSpeeds(-EXP_SPEED + 50, EXP_SPEED); // Move back in an arc going to the left
                // if rear sensors detect an object as well, then turn on spot
                if ((proxValues[3] < 20) || (proxValues[4] < 20)) {
                    setMotorSpeeds(EXP_SPEED, -EXP_SPEED);
                }
            } // if object is on the right and obstructing path, turn left
            else if ((proxValues[0] < 100) || (proxValues[1] < 20) || (proxValues[2] < 10)) {
                setMotorSpeeds(0, EXP_SPEED);
            } // if object is on the left and obstructing path, turn right
            else if ((proxValues[7] < 100) || (proxValues[6] < 20) || (proxValues[5] < 10)) {
                setMotorSpeeds(EXP_SPEED, 0);
            } // if no object in front, proceed at exploration speed
            else {
                setMotorSpeeds(EXP_SPEED, EXP_SPEED);
            }
        }
        delay(100); // delay to prevent spurious loops
    }
}

/* Thread: Task 2 */
void task2(void){
    uint selectorValue = 0;                     // selector value
    uint proxValues[8];                         // proximity sensor values
    uint minLeftValue, minRightValue = 65535;   // closest object distance
    int i = 0;                                  // loop variable
    
    while(true) {
        if(SR & 0x00E0){           
            SRbits.IPL = 0;
        }
        if(CORCON & 0x08){
            CORCONbits.IPL3 = 0;
        }
        
        selectorValue = Sys_Get_Selector(); // get selector value
        if(selectorValue == 2) {            // if task 2 selected
            // get proximity sensor values
            for(i = 0; i < 8; i++) {
                proxValues[i] = Sys_Get_Prox(i);
            }
            
            // if object is in cross hairs
            if((proxValues[0] <= OBJ_THRESHOLD_MAX) || (proxValues[7] <= OBJ_THRESHOLD_MAX)) {
                if(proxValues[0] <= OBJ_THRESHOLD_MIN || proxValues[7] <= OBJ_THRESHOLD_MIN) { // too close to object
                    setMotorSpeeds(0, 0); // stop motors
                } else {
                    if(proxValues[0] < proxValues[7]) {
                        setMotorSpeeds(OBJ_FOLLOW_SPEED, 0); // turn right
                    } else if(proxValues[7] < proxValues[0]) {
                        setMotorSpeeds(0, OBJ_FOLLOW_SPEED); // turn left
                    } else { // object in front
                        setMotorSpeeds(OBJ_FOLLOW_SPEED, OBJ_FOLLOW_SPEED);
                    }
                }
            } else {
                // check if object is detected by any other sensors
                // if object on the right side of robot
                for(i = 1; i < 4; i++) {
                    if(proxValues[i] < minRightValue) minRightValue = proxValues[i];
                }
                
                // if object on the left side of robot
                for(i = 4; i < 7; i++) {
                    if(proxValues[i] < minLeftValue) minLeftValue = proxValues[i];
                }
                
                if ((minLeftValue == 65535) && (minRightValue == 65535)) {
                    // no object detected
                    setMotorSpeeds(0, 0);
                } else if(minLeftValue <= minRightValue) {
                    // closest object on left - spin anti-clockwise
                    setMotorSpeeds(-SPIN_SPEED, SPIN_SPEED);                    
                } else if(minRightValue < minLeftValue) {
                    // closest object on right - spin clockwise
                    setMotorSpeeds(SPIN_SPEED, -SPIN_SPEED);
                }
                
                // reset closest object variables
                minLeftValue = 65535;
                minRightValue = 65535;
            }
        }
        delay(100); // delay to prevent spurious loops
    }
}

/* Thread: No Task Selected */
void defaultThread(void) {
    uint selectorValue = 0;
    
    while(true) {
        selectorValue = Sys_Get_Selector();
        if((selectorValue != 1) && (selectorValue != 2)) {
            // turn motors off
            Sys_Send_IntEvent(SYS_EVENT_IO_MOTOR_LEFT, 0);
            Sys_Send_IntEvent(SYS_EVENT_IO_MOTOR_RIGHT, 0);
            
            // turn all LEDs off
            LED0 = 0;
            LED1 = 0;
            LED2 = 0;
            LED3 = 0;
            LED4 = 0;
            LED5 = 0;
            LED6 = 0;
            LED7 = 0;
            FRONT_LED = 0;
            
            // toggle body LED
            BODY_LED = ~BODY_LED;
            delay(250);
        }
    }
}

/* Blocking delay (ms) */
void delay(uint32 value) {
    uint32 time = Sys_Get_SystemClock() + value;
    uint32 time_now = Sys_Get_SystemClock();
             
    while(time_now < time) {
        time_now = Sys_Get_SystemClock();//get the current time
    }
}

/* Object detected indicator */
void objNDetInd(uint8 data) {
    switch (data) {
        case 0:
            LED0 = 0;
            break;
        case 1:
            LED1 = 0;
            break;
        case 2:
            LED2 = 0;
            break;
        case 3:
            LED3 = 0;
            break;
        case 4:
            LED5 = 0;
            break;
        case 5:
            LED6 = 0;
            break;
        case 6:
            LED7 = 0;
            break;
        case 7:
            break;
            LED0 = 0;
    }
}

/* Object not detected indicator */
void objDetInd(uint8 data) {
    switch (data) {
        case 0:
            LED0 = 1;
            break;
        case 1:
            LED1 = 1;
            break;
        case 2:
            LED2 = 1;
            break;
        case 3:
            LED3 = 1;
            break;
        case 4:
            LED5 = 1;
            break;
        case 5:
            LED6 = 1;
            break;
        case 6:
            LED7 = 1;
            break;
        case 7:
            break;
            LED0 = 1;
    }
}

/* Sets motor speeds */
void setMotorSpeeds(sint16 leftMotorSpeed, sint16 rightMotorSpeed) {
    // saturate values
    if(leftMotorSpeed > 127) leftMotorSpeed = 127;
    if(rightMotorSpeed > 127) rightMotorSpeed = 127;
    if(leftMotorSpeed < -127) leftMotorSpeed = -127;
    if(rightMotorSpeed < -127) rightMotorSpeed = -127;
    
    // set motor speeds
    Sys_Send_IntEvent(SYS_EVENT_IO_MOTOR_LEFT, leftMotorSpeed);
    Sys_Send_IntEvent(SYS_EVENT_IO_MOTOR_RIGHT, rightMotorSpeed);
}