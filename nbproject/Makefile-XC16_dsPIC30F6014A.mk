#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-XC16_dsPIC30F6014A.mk)" "nbproject/Makefile-local-XC16_dsPIC30F6014A.mk"
include nbproject/Makefile-local-XC16_dsPIC30F6014A.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=XC16_dsPIC30F6014A
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/master.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/master.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=os/platform/e-puck/library/acc_gyro/e_lsm330.c os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.c os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.c os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.c os/platform/e-puck/library/camera/fast_2_timer/e_common.c os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.s os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.c os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.c os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.c os/platform/e-puck/library/camera/fast_2_timer/e_timers.c os/platform/e-puck/library/I2C/e_I2C_protocol.c os/platform/e-puck/library/I2C/e_I2C_master_module.c os/platform/e-puck/library/motor_led/e_init_port.c os/events/events.c os/io/io.c os/platform/e-puck/camera.c os/platform/e-puck/i2c.c os/platform/e-puck/i2c_data.c os/platform/e-puck/motors.c os/platform/e-puck/remoteControl.c os/platform/e-puck/uart.c os/io/io_clock.c os/io/e-puck/selector.c os/platform/e-puck/adc.c os/platform/e-puck/proximity.c os/platform/e-puck/system_Timer_HDI.c os/platform/e-puck/camera_processing.c os/platform/e-puck/traps.c os/platform/e-puck/i2c_HDI.c os/platform/e-puck/io_HDI.c os/platform/e-puck/motors_HDI.c os/platform/e-puck/process_Management_HDI.c os/platform/e-puck/remoteControl_HDI.c os/platform/e-puck/uart_HDI.c os/processes/data.c os/processes/process_Management.c os/processes/scheduler.c os/processes/system_Timer.c os/interrupts.c os/memory.c os/system.c main.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/os/platform/e-puck/library/acc_gyro/e_lsm330.o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_common.o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_timers.o ${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_protocol.o ${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_master_module.o ${OBJECTDIR}/os/platform/e-puck/library/motor_led/e_init_port.o ${OBJECTDIR}/os/events/events.o ${OBJECTDIR}/os/io/io.o ${OBJECTDIR}/os/platform/e-puck/camera.o ${OBJECTDIR}/os/platform/e-puck/i2c.o ${OBJECTDIR}/os/platform/e-puck/i2c_data.o ${OBJECTDIR}/os/platform/e-puck/motors.o ${OBJECTDIR}/os/platform/e-puck/remoteControl.o ${OBJECTDIR}/os/platform/e-puck/uart.o ${OBJECTDIR}/os/io/io_clock.o ${OBJECTDIR}/os/io/e-puck/selector.o ${OBJECTDIR}/os/platform/e-puck/adc.o ${OBJECTDIR}/os/platform/e-puck/proximity.o ${OBJECTDIR}/os/platform/e-puck/system_Timer_HDI.o ${OBJECTDIR}/os/platform/e-puck/camera_processing.o ${OBJECTDIR}/os/platform/e-puck/traps.o ${OBJECTDIR}/os/platform/e-puck/i2c_HDI.o ${OBJECTDIR}/os/platform/e-puck/io_HDI.o ${OBJECTDIR}/os/platform/e-puck/motors_HDI.o ${OBJECTDIR}/os/platform/e-puck/process_Management_HDI.o ${OBJECTDIR}/os/platform/e-puck/remoteControl_HDI.o ${OBJECTDIR}/os/platform/e-puck/uart_HDI.o ${OBJECTDIR}/os/processes/data.o ${OBJECTDIR}/os/processes/process_Management.o ${OBJECTDIR}/os/processes/scheduler.o ${OBJECTDIR}/os/processes/system_Timer.o ${OBJECTDIR}/os/interrupts.o ${OBJECTDIR}/os/memory.o ${OBJECTDIR}/os/system.o ${OBJECTDIR}/main.o
POSSIBLE_DEPFILES=${OBJECTDIR}/os/platform/e-puck/library/acc_gyro/e_lsm330.o.d ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.o.d ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.o.d ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.o.d ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_common.o.d ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.o.d ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.o.d ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.o.d ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.o.d ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_timers.o.d ${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_protocol.o.d ${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_master_module.o.d ${OBJECTDIR}/os/platform/e-puck/library/motor_led/e_init_port.o.d ${OBJECTDIR}/os/events/events.o.d ${OBJECTDIR}/os/io/io.o.d ${OBJECTDIR}/os/platform/e-puck/camera.o.d ${OBJECTDIR}/os/platform/e-puck/i2c.o.d ${OBJECTDIR}/os/platform/e-puck/i2c_data.o.d ${OBJECTDIR}/os/platform/e-puck/motors.o.d ${OBJECTDIR}/os/platform/e-puck/remoteControl.o.d ${OBJECTDIR}/os/platform/e-puck/uart.o.d ${OBJECTDIR}/os/io/io_clock.o.d ${OBJECTDIR}/os/io/e-puck/selector.o.d ${OBJECTDIR}/os/platform/e-puck/adc.o.d ${OBJECTDIR}/os/platform/e-puck/proximity.o.d ${OBJECTDIR}/os/platform/e-puck/system_Timer_HDI.o.d ${OBJECTDIR}/os/platform/e-puck/camera_processing.o.d ${OBJECTDIR}/os/platform/e-puck/traps.o.d ${OBJECTDIR}/os/platform/e-puck/i2c_HDI.o.d ${OBJECTDIR}/os/platform/e-puck/io_HDI.o.d ${OBJECTDIR}/os/platform/e-puck/motors_HDI.o.d ${OBJECTDIR}/os/platform/e-puck/process_Management_HDI.o.d ${OBJECTDIR}/os/platform/e-puck/remoteControl_HDI.o.d ${OBJECTDIR}/os/platform/e-puck/uart_HDI.o.d ${OBJECTDIR}/os/processes/data.o.d ${OBJECTDIR}/os/processes/process_Management.o.d ${OBJECTDIR}/os/processes/scheduler.o.d ${OBJECTDIR}/os/processes/system_Timer.o.d ${OBJECTDIR}/os/interrupts.o.d ${OBJECTDIR}/os/memory.o.d ${OBJECTDIR}/os/system.o.d ${OBJECTDIR}/main.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/os/platform/e-puck/library/acc_gyro/e_lsm330.o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_common.o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_timers.o ${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_protocol.o ${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_master_module.o ${OBJECTDIR}/os/platform/e-puck/library/motor_led/e_init_port.o ${OBJECTDIR}/os/events/events.o ${OBJECTDIR}/os/io/io.o ${OBJECTDIR}/os/platform/e-puck/camera.o ${OBJECTDIR}/os/platform/e-puck/i2c.o ${OBJECTDIR}/os/platform/e-puck/i2c_data.o ${OBJECTDIR}/os/platform/e-puck/motors.o ${OBJECTDIR}/os/platform/e-puck/remoteControl.o ${OBJECTDIR}/os/platform/e-puck/uart.o ${OBJECTDIR}/os/io/io_clock.o ${OBJECTDIR}/os/io/e-puck/selector.o ${OBJECTDIR}/os/platform/e-puck/adc.o ${OBJECTDIR}/os/platform/e-puck/proximity.o ${OBJECTDIR}/os/platform/e-puck/system_Timer_HDI.o ${OBJECTDIR}/os/platform/e-puck/camera_processing.o ${OBJECTDIR}/os/platform/e-puck/traps.o ${OBJECTDIR}/os/platform/e-puck/i2c_HDI.o ${OBJECTDIR}/os/platform/e-puck/io_HDI.o ${OBJECTDIR}/os/platform/e-puck/motors_HDI.o ${OBJECTDIR}/os/platform/e-puck/process_Management_HDI.o ${OBJECTDIR}/os/platform/e-puck/remoteControl_HDI.o ${OBJECTDIR}/os/platform/e-puck/uart_HDI.o ${OBJECTDIR}/os/processes/data.o ${OBJECTDIR}/os/processes/process_Management.o ${OBJECTDIR}/os/processes/scheduler.o ${OBJECTDIR}/os/processes/system_Timer.o ${OBJECTDIR}/os/interrupts.o ${OBJECTDIR}/os/memory.o ${OBJECTDIR}/os/system.o ${OBJECTDIR}/main.o

# Source Files
SOURCEFILES=os/platform/e-puck/library/acc_gyro/e_lsm330.c os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.c os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.c os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.c os/platform/e-puck/library/camera/fast_2_timer/e_common.c os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.s os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.c os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.c os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.c os/platform/e-puck/library/camera/fast_2_timer/e_timers.c os/platform/e-puck/library/I2C/e_I2C_protocol.c os/platform/e-puck/library/I2C/e_I2C_master_module.c os/platform/e-puck/library/motor_led/e_init_port.c os/events/events.c os/io/io.c os/platform/e-puck/camera.c os/platform/e-puck/i2c.c os/platform/e-puck/i2c_data.c os/platform/e-puck/motors.c os/platform/e-puck/remoteControl.c os/platform/e-puck/uart.c os/io/io_clock.c os/io/e-puck/selector.c os/platform/e-puck/adc.c os/platform/e-puck/proximity.c os/platform/e-puck/system_Timer_HDI.c os/platform/e-puck/camera_processing.c os/platform/e-puck/traps.c os/platform/e-puck/i2c_HDI.c os/platform/e-puck/io_HDI.c os/platform/e-puck/motors_HDI.c os/platform/e-puck/process_Management_HDI.c os/platform/e-puck/remoteControl_HDI.c os/platform/e-puck/uart_HDI.c os/processes/data.c os/processes/process_Management.c os/processes/scheduler.c os/processes/system_Timer.c os/interrupts.c os/memory.c os/system.c main.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-XC16_dsPIC30F6014A.mk dist/${CND_CONF}/${IMAGE_TYPE}/master.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=30F6014A
MP_LINKER_FILE_OPTION=,--script=p30F6014A.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/os/platform/e-puck/library/acc_gyro/e_lsm330.o: os/platform/e-puck/library/acc_gyro/e_lsm330.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/acc_gyro" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/acc_gyro/e_lsm330.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/acc_gyro/e_lsm330.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/acc_gyro/e_lsm330.c  -o ${OBJECTDIR}/os/platform/e-puck/library/acc_gyro/e_lsm330.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/acc_gyro/e_lsm330.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/acc_gyro/e_lsm330.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.o: os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.c  -o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.o: os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.c  -o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.o: os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.c  -o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_common.o: os/platform/e-puck/library/camera/fast_2_timer/e_common.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_common.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_common.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/camera/fast_2_timer/e_common.c  -o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_common.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_common.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_common.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.o: os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.c  -o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.o: os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.c  -o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.o: os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.c  -o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_timers.o: os/platform/e-puck/library/camera/fast_2_timer/e_timers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_timers.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_timers.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/camera/fast_2_timer/e_timers.c  -o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_timers.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_timers.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_timers.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_protocol.o: os/platform/e-puck/library/I2C/e_I2C_protocol.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/I2C" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_protocol.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_protocol.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/I2C/e_I2C_protocol.c  -o ${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_protocol.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_protocol.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_protocol.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_master_module.o: os/platform/e-puck/library/I2C/e_I2C_master_module.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/I2C" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_master_module.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_master_module.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/I2C/e_I2C_master_module.c  -o ${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_master_module.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_master_module.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_master_module.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/motor_led/e_init_port.o: os/platform/e-puck/library/motor_led/e_init_port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/motor_led" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/motor_led/e_init_port.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/motor_led/e_init_port.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/motor_led/e_init_port.c  -o ${OBJECTDIR}/os/platform/e-puck/library/motor_led/e_init_port.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/motor_led/e_init_port.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/motor_led/e_init_port.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/events/events.o: os/events/events.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/events" 
	@${RM} ${OBJECTDIR}/os/events/events.o.d 
	@${RM} ${OBJECTDIR}/os/events/events.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/events/events.c  -o ${OBJECTDIR}/os/events/events.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/events/events.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/events/events.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/io/io.o: os/io/io.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/io" 
	@${RM} ${OBJECTDIR}/os/io/io.o.d 
	@${RM} ${OBJECTDIR}/os/io/io.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/io/io.c  -o ${OBJECTDIR}/os/io/io.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/io/io.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/io/io.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/camera.o: os/platform/e-puck/camera.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/camera.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/camera.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/camera.c  -o ${OBJECTDIR}/os/platform/e-puck/camera.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/camera.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/camera.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/i2c.o: os/platform/e-puck/i2c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/i2c.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/i2c.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/i2c.c  -o ${OBJECTDIR}/os/platform/e-puck/i2c.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/i2c.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/i2c.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/i2c_data.o: os/platform/e-puck/i2c_data.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/i2c_data.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/i2c_data.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/i2c_data.c  -o ${OBJECTDIR}/os/platform/e-puck/i2c_data.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/i2c_data.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/i2c_data.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/motors.o: os/platform/e-puck/motors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/motors.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/motors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/motors.c  -o ${OBJECTDIR}/os/platform/e-puck/motors.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/motors.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/motors.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/remoteControl.o: os/platform/e-puck/remoteControl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/remoteControl.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/remoteControl.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/remoteControl.c  -o ${OBJECTDIR}/os/platform/e-puck/remoteControl.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/remoteControl.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/remoteControl.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/uart.o: os/platform/e-puck/uart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/uart.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/uart.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/uart.c  -o ${OBJECTDIR}/os/platform/e-puck/uart.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/uart.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/uart.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/io/io_clock.o: os/io/io_clock.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/io" 
	@${RM} ${OBJECTDIR}/os/io/io_clock.o.d 
	@${RM} ${OBJECTDIR}/os/io/io_clock.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/io/io_clock.c  -o ${OBJECTDIR}/os/io/io_clock.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/io/io_clock.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/io/io_clock.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/io/e-puck/selector.o: os/io/e-puck/selector.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/io/e-puck" 
	@${RM} ${OBJECTDIR}/os/io/e-puck/selector.o.d 
	@${RM} ${OBJECTDIR}/os/io/e-puck/selector.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/io/e-puck/selector.c  -o ${OBJECTDIR}/os/io/e-puck/selector.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/io/e-puck/selector.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/io/e-puck/selector.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/adc.o: os/platform/e-puck/adc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/adc.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/adc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/adc.c  -o ${OBJECTDIR}/os/platform/e-puck/adc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/adc.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/adc.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/proximity.o: os/platform/e-puck/proximity.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/proximity.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/proximity.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/proximity.c  -o ${OBJECTDIR}/os/platform/e-puck/proximity.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/proximity.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/proximity.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/system_Timer_HDI.o: os/platform/e-puck/system_Timer_HDI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/system_Timer_HDI.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/system_Timer_HDI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/system_Timer_HDI.c  -o ${OBJECTDIR}/os/platform/e-puck/system_Timer_HDI.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/system_Timer_HDI.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/system_Timer_HDI.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/camera_processing.o: os/platform/e-puck/camera_processing.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/camera_processing.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/camera_processing.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/camera_processing.c  -o ${OBJECTDIR}/os/platform/e-puck/camera_processing.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/camera_processing.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/camera_processing.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/traps.o: os/platform/e-puck/traps.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/traps.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/traps.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/traps.c  -o ${OBJECTDIR}/os/platform/e-puck/traps.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/traps.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/traps.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/i2c_HDI.o: os/platform/e-puck/i2c_HDI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/i2c_HDI.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/i2c_HDI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/i2c_HDI.c  -o ${OBJECTDIR}/os/platform/e-puck/i2c_HDI.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/i2c_HDI.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/i2c_HDI.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/io_HDI.o: os/platform/e-puck/io_HDI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/io_HDI.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/io_HDI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/io_HDI.c  -o ${OBJECTDIR}/os/platform/e-puck/io_HDI.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/io_HDI.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/io_HDI.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/motors_HDI.o: os/platform/e-puck/motors_HDI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/motors_HDI.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/motors_HDI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/motors_HDI.c  -o ${OBJECTDIR}/os/platform/e-puck/motors_HDI.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/motors_HDI.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/motors_HDI.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/process_Management_HDI.o: os/platform/e-puck/process_Management_HDI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/process_Management_HDI.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/process_Management_HDI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/process_Management_HDI.c  -o ${OBJECTDIR}/os/platform/e-puck/process_Management_HDI.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/process_Management_HDI.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/process_Management_HDI.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/remoteControl_HDI.o: os/platform/e-puck/remoteControl_HDI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/remoteControl_HDI.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/remoteControl_HDI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/remoteControl_HDI.c  -o ${OBJECTDIR}/os/platform/e-puck/remoteControl_HDI.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/remoteControl_HDI.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/remoteControl_HDI.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/uart_HDI.o: os/platform/e-puck/uart_HDI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/uart_HDI.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/uart_HDI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/uart_HDI.c  -o ${OBJECTDIR}/os/platform/e-puck/uart_HDI.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/uart_HDI.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/uart_HDI.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/processes/data.o: os/processes/data.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/processes" 
	@${RM} ${OBJECTDIR}/os/processes/data.o.d 
	@${RM} ${OBJECTDIR}/os/processes/data.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/processes/data.c  -o ${OBJECTDIR}/os/processes/data.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/processes/data.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/processes/data.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/processes/process_Management.o: os/processes/process_Management.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/processes" 
	@${RM} ${OBJECTDIR}/os/processes/process_Management.o.d 
	@${RM} ${OBJECTDIR}/os/processes/process_Management.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/processes/process_Management.c  -o ${OBJECTDIR}/os/processes/process_Management.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/processes/process_Management.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/processes/process_Management.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/processes/scheduler.o: os/processes/scheduler.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/processes" 
	@${RM} ${OBJECTDIR}/os/processes/scheduler.o.d 
	@${RM} ${OBJECTDIR}/os/processes/scheduler.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/processes/scheduler.c  -o ${OBJECTDIR}/os/processes/scheduler.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/processes/scheduler.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/processes/scheduler.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/processes/system_Timer.o: os/processes/system_Timer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/processes" 
	@${RM} ${OBJECTDIR}/os/processes/system_Timer.o.d 
	@${RM} ${OBJECTDIR}/os/processes/system_Timer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/processes/system_Timer.c  -o ${OBJECTDIR}/os/processes/system_Timer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/processes/system_Timer.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/processes/system_Timer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/interrupts.o: os/interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os" 
	@${RM} ${OBJECTDIR}/os/interrupts.o.d 
	@${RM} ${OBJECTDIR}/os/interrupts.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/interrupts.c  -o ${OBJECTDIR}/os/interrupts.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/interrupts.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/interrupts.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/memory.o: os/memory.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os" 
	@${RM} ${OBJECTDIR}/os/memory.o.d 
	@${RM} ${OBJECTDIR}/os/memory.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/memory.c  -o ${OBJECTDIR}/os/memory.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/memory.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/memory.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/system.o: os/system.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os" 
	@${RM} ${OBJECTDIR}/os/system.o.d 
	@${RM} ${OBJECTDIR}/os/system.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/system.c  -o ${OBJECTDIR}/os/system.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/system.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/system.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/main.o: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  main.c  -o ${OBJECTDIR}/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/main.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/os/platform/e-puck/library/acc_gyro/e_lsm330.o: os/platform/e-puck/library/acc_gyro/e_lsm330.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/acc_gyro" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/acc_gyro/e_lsm330.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/acc_gyro/e_lsm330.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/acc_gyro/e_lsm330.c  -o ${OBJECTDIR}/os/platform/e-puck/library/acc_gyro/e_lsm330.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/acc_gyro/e_lsm330.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/acc_gyro/e_lsm330.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.o: os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.c  -o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po3030k.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.o: os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.c  -o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po6030k.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.o: os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.c  -o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_calc_po8030d.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_common.o: os/platform/e-puck/library/camera/fast_2_timer/e_common.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_common.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_common.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/camera/fast_2_timer/e_common.c  -o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_common.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_common.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_common.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.o: os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.c  -o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po3030k_registers.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.o: os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.c  -o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po6030k_registers.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.o: os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.c  -o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_po8030d_registers.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_timers.o: os/platform/e-puck/library/camera/fast_2_timer/e_timers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_timers.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_timers.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/camera/fast_2_timer/e_timers.c  -o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_timers.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_timers.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_timers.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_protocol.o: os/platform/e-puck/library/I2C/e_I2C_protocol.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/I2C" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_protocol.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_protocol.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/I2C/e_I2C_protocol.c  -o ${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_protocol.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_protocol.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_protocol.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_master_module.o: os/platform/e-puck/library/I2C/e_I2C_master_module.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/I2C" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_master_module.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_master_module.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/I2C/e_I2C_master_module.c  -o ${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_master_module.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_master_module.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/I2C/e_I2C_master_module.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/library/motor_led/e_init_port.o: os/platform/e-puck/library/motor_led/e_init_port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/motor_led" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/motor_led/e_init_port.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/motor_led/e_init_port.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/library/motor_led/e_init_port.c  -o ${OBJECTDIR}/os/platform/e-puck/library/motor_led/e_init_port.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/library/motor_led/e_init_port.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/motor_led/e_init_port.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/events/events.o: os/events/events.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/events" 
	@${RM} ${OBJECTDIR}/os/events/events.o.d 
	@${RM} ${OBJECTDIR}/os/events/events.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/events/events.c  -o ${OBJECTDIR}/os/events/events.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/events/events.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/events/events.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/io/io.o: os/io/io.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/io" 
	@${RM} ${OBJECTDIR}/os/io/io.o.d 
	@${RM} ${OBJECTDIR}/os/io/io.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/io/io.c  -o ${OBJECTDIR}/os/io/io.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/io/io.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/io/io.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/camera.o: os/platform/e-puck/camera.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/camera.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/camera.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/camera.c  -o ${OBJECTDIR}/os/platform/e-puck/camera.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/camera.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/camera.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/i2c.o: os/platform/e-puck/i2c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/i2c.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/i2c.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/i2c.c  -o ${OBJECTDIR}/os/platform/e-puck/i2c.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/i2c.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/i2c.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/i2c_data.o: os/platform/e-puck/i2c_data.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/i2c_data.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/i2c_data.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/i2c_data.c  -o ${OBJECTDIR}/os/platform/e-puck/i2c_data.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/i2c_data.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/i2c_data.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/motors.o: os/platform/e-puck/motors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/motors.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/motors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/motors.c  -o ${OBJECTDIR}/os/platform/e-puck/motors.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/motors.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/motors.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/remoteControl.o: os/platform/e-puck/remoteControl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/remoteControl.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/remoteControl.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/remoteControl.c  -o ${OBJECTDIR}/os/platform/e-puck/remoteControl.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/remoteControl.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/remoteControl.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/uart.o: os/platform/e-puck/uart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/uart.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/uart.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/uart.c  -o ${OBJECTDIR}/os/platform/e-puck/uart.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/uart.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/uart.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/io/io_clock.o: os/io/io_clock.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/io" 
	@${RM} ${OBJECTDIR}/os/io/io_clock.o.d 
	@${RM} ${OBJECTDIR}/os/io/io_clock.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/io/io_clock.c  -o ${OBJECTDIR}/os/io/io_clock.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/io/io_clock.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/io/io_clock.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/io/e-puck/selector.o: os/io/e-puck/selector.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/io/e-puck" 
	@${RM} ${OBJECTDIR}/os/io/e-puck/selector.o.d 
	@${RM} ${OBJECTDIR}/os/io/e-puck/selector.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/io/e-puck/selector.c  -o ${OBJECTDIR}/os/io/e-puck/selector.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/io/e-puck/selector.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/io/e-puck/selector.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/adc.o: os/platform/e-puck/adc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/adc.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/adc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/adc.c  -o ${OBJECTDIR}/os/platform/e-puck/adc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/adc.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/adc.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/proximity.o: os/platform/e-puck/proximity.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/proximity.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/proximity.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/proximity.c  -o ${OBJECTDIR}/os/platform/e-puck/proximity.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/proximity.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/proximity.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/system_Timer_HDI.o: os/platform/e-puck/system_Timer_HDI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/system_Timer_HDI.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/system_Timer_HDI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/system_Timer_HDI.c  -o ${OBJECTDIR}/os/platform/e-puck/system_Timer_HDI.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/system_Timer_HDI.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/system_Timer_HDI.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/camera_processing.o: os/platform/e-puck/camera_processing.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/camera_processing.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/camera_processing.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/camera_processing.c  -o ${OBJECTDIR}/os/platform/e-puck/camera_processing.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/camera_processing.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/camera_processing.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/traps.o: os/platform/e-puck/traps.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/traps.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/traps.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/traps.c  -o ${OBJECTDIR}/os/platform/e-puck/traps.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/traps.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/traps.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/i2c_HDI.o: os/platform/e-puck/i2c_HDI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/i2c_HDI.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/i2c_HDI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/i2c_HDI.c  -o ${OBJECTDIR}/os/platform/e-puck/i2c_HDI.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/i2c_HDI.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/i2c_HDI.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/io_HDI.o: os/platform/e-puck/io_HDI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/io_HDI.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/io_HDI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/io_HDI.c  -o ${OBJECTDIR}/os/platform/e-puck/io_HDI.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/io_HDI.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/io_HDI.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/motors_HDI.o: os/platform/e-puck/motors_HDI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/motors_HDI.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/motors_HDI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/motors_HDI.c  -o ${OBJECTDIR}/os/platform/e-puck/motors_HDI.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/motors_HDI.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/motors_HDI.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/process_Management_HDI.o: os/platform/e-puck/process_Management_HDI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/process_Management_HDI.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/process_Management_HDI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/process_Management_HDI.c  -o ${OBJECTDIR}/os/platform/e-puck/process_Management_HDI.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/process_Management_HDI.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/process_Management_HDI.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/remoteControl_HDI.o: os/platform/e-puck/remoteControl_HDI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/remoteControl_HDI.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/remoteControl_HDI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/remoteControl_HDI.c  -o ${OBJECTDIR}/os/platform/e-puck/remoteControl_HDI.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/remoteControl_HDI.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/remoteControl_HDI.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/platform/e-puck/uart_HDI.o: os/platform/e-puck/uart_HDI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/uart_HDI.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/uart_HDI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/platform/e-puck/uart_HDI.c  -o ${OBJECTDIR}/os/platform/e-puck/uart_HDI.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/platform/e-puck/uart_HDI.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/uart_HDI.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/processes/data.o: os/processes/data.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/processes" 
	@${RM} ${OBJECTDIR}/os/processes/data.o.d 
	@${RM} ${OBJECTDIR}/os/processes/data.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/processes/data.c  -o ${OBJECTDIR}/os/processes/data.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/processes/data.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/processes/data.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/processes/process_Management.o: os/processes/process_Management.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/processes" 
	@${RM} ${OBJECTDIR}/os/processes/process_Management.o.d 
	@${RM} ${OBJECTDIR}/os/processes/process_Management.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/processes/process_Management.c  -o ${OBJECTDIR}/os/processes/process_Management.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/processes/process_Management.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/processes/process_Management.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/processes/scheduler.o: os/processes/scheduler.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/processes" 
	@${RM} ${OBJECTDIR}/os/processes/scheduler.o.d 
	@${RM} ${OBJECTDIR}/os/processes/scheduler.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/processes/scheduler.c  -o ${OBJECTDIR}/os/processes/scheduler.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/processes/scheduler.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/processes/scheduler.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/processes/system_Timer.o: os/processes/system_Timer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/processes" 
	@${RM} ${OBJECTDIR}/os/processes/system_Timer.o.d 
	@${RM} ${OBJECTDIR}/os/processes/system_Timer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/processes/system_Timer.c  -o ${OBJECTDIR}/os/processes/system_Timer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/processes/system_Timer.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/processes/system_Timer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/interrupts.o: os/interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os" 
	@${RM} ${OBJECTDIR}/os/interrupts.o.d 
	@${RM} ${OBJECTDIR}/os/interrupts.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/interrupts.c  -o ${OBJECTDIR}/os/interrupts.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/interrupts.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/interrupts.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/memory.o: os/memory.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os" 
	@${RM} ${OBJECTDIR}/os/memory.o.d 
	@${RM} ${OBJECTDIR}/os/memory.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/memory.c  -o ${OBJECTDIR}/os/memory.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/memory.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/memory.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/os/system.o: os/system.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os" 
	@${RM} ${OBJECTDIR}/os/system.o.d 
	@${RM} ${OBJECTDIR}/os/system.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  os/system.c  -o ${OBJECTDIR}/os/system.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/os/system.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/os/system.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/main.o: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  main.c  -o ${OBJECTDIR}/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/main.o.d"        -g -omf=elf -no-legacy-libc  -O0 -msmart-io=1 -Wall -msfr-warn=on   -fms-extensions
	@${FIXDEPS} "${OBJECTDIR}/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.o: os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.s  -o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -omf=elf -no-legacy-libc  -Wa,-MD,"${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.o.d",--defsym=__MPLAB_BUILD=1,--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.o: os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer" 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.o.d 
	@${RM} ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.s  -o ${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -omf=elf -no-legacy-libc  -Wa,-MD,"${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.o.d",--defsym=__MPLAB_BUILD=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/os/platform/e-puck/library/camera/fast_2_timer/e_interrupt.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/master.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/master.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -omf=elf -no-legacy-libc   -mreserve=data@0x800:0x81F -mreserve=data@0x820:0x821 -mreserve=data@0x822:0x823 -mreserve=data@0x824:0x84F   -Wl,,--defsym=__MPLAB_BUILD=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,$(MP_LINKER_FILE_OPTION),--heap=4112,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST) 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/master.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/master.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -omf=elf -no-legacy-libc  -Wl,,--defsym=__MPLAB_BUILD=1,$(MP_LINKER_FILE_OPTION),--heap=4112,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST) 
	${MP_CC_DIR}\\xc16-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/master.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -a  -omf=elf  
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/XC16_dsPIC30F6014A
	${RM} -r dist/XC16_dsPIC30F6014A

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
